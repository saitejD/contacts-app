import 'package:contacts/app_contact_model.dart';
import 'package:flutter/material.dart';

class ContactAvatar extends StatelessWidget {
  final AppContact contact;
  final double size;
  const ContactAvatar({Key? key, required this.contact, required this.size})
      : super(key: key);

  LinearGradient getColorGradient(Color color) {
    var baseColor = color as dynamic;
    Color color1 = baseColor[800];
    Color color2 = baseColor[400];
    return LinearGradient(
      colors: [
        color1,
        color2,
      ],
      begin: Alignment.bottomLeft,
      end: Alignment.topRight,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        gradient: getColorGradient(contact.color),
      ),
      child: (contact.info.avatar != null && contact.info.avatar!.length > 0)
          ? CircleAvatar(
              backgroundImage: MemoryImage(contact.info.avatar!),
            )
          : CircleAvatar(
              child: Text(
                contact.info.initials(),
                style: TextStyle(
                    color: Colors.white, backgroundColor: Colors.transparent),
              ),
            ),
    );
  }
}
