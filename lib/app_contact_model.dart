import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/rendering.dart';

class AppContact {
  final Color color;
  late final Contact info;

  AppContact({required this.color, required this.info});
}
