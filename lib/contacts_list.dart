import 'package:contacts/app_contact_model.dart';
import 'package:contacts/contact_avatar.dart';
import 'package:contacts/contact_details_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ContactsList extends StatelessWidget {
  final List<AppContact> contacts;
  final Function() reloadContacts;
  ContactsList({required this.contacts, required this.reloadContacts});
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: contacts.length,
      itemBuilder: (context, index) {
        AppContact contact = contacts[index];

        return ListTile(
            onTap: () {
              Navigator.of(context).push(
                CupertinoPageRoute(
                  builder: (BuildContext context) => ContactDetailsScreen(
                    contact: contact,
                    onContactDelete: (AppContact _contact) {
                      reloadContacts();
                      Navigator.of(context).pop();
                    },
                    onContactUpdate: (AppContact _contact) {
                      reloadContacts();
                    },
                  ),
                ),
              );
            },
            title: Text(contact.info.displayName != null
                ? contact.info.displayName!
                : "No Name"),
            subtitle: Text(contact.info.phones!.length > 0
                ? contact.info.phones!.elementAt(0).value!
                : ''),
            leading: ContactAvatar(contact: contact, size: 36));
      },
    );
  }
}
