# Contacts App

This a general contacts app which we often find in our phones. It shows all contacts saved in your phone, you can create a new contact, modify a contact, delete a contact etc.

# App UI

<img src="UI/Screenshot_1641552198.png" width="300"></br>

<img src="UI/Screenshot_1641552221.png" width="300"></br>

<img src="UI/Screenshot_1641552210.png" width="300"></br>

<img src="UI/Screenshot_1641552224.png" width="300"></br>
